# Disk partionning
# Partion type : EFI (ef00) / Linux FS (8300)
gdisk /dev/sda

# Formatting partitions
mkfs.vfat /dev/sda1
mkfs.btrfs /dev/sda3

mkswap /dev/sda2
swapon /dev/sda2

# Btrfs subvolumes creation
mount /dev/sda3 /mnt
cd /mnt
btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @var
cd
umount /mnt

# Volumes re-mount
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@ /dev/sda3 /mnt
mkdir /mnt/{boot,home,var}
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@home /dev/sda3 /mnt/home
mount -o noatime,compress=zstd,space_cache,discard=async,subvol=@var /dev/sda3 /mnt/var
mount /dev/sda1 /mnt/boot

# Base install
pacstrap /mnt base linux linux-firmware git nano vim intel-ucode btrfs-progs

genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt

git clone https://gitlab.com/sheamair/config-archlinux
